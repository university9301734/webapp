package GNNF.backend.database;

import GNNF.backend.resources.*;
import GNNF.backend.util.SQLStatements;

import java.sql.*;
import java.util.ArrayList;

//TODO: may also consider of creating a separate class for every possible insertion

/**
 * Insert into the Database a new resource.
 */
public final class CreateDBEntry
{
    private final Connection conn;

    /**
     * Inserts some resources into the database.
     * @param conn the connection to the database. The connection gets CLOSED either upon successful or unsuccessful creation of the resource.
     */
    public CreateDBEntry(final Connection conn)
    {
        this.conn = conn;
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a list of new Categories into the database.
     */
    public void newCategory(final ArrayList<Category> categories) throws SQLException
    {
        PreparedStatement p = null;
        try
        {
            p = conn.prepareStatement(SQLStatements.INSERT_CATEGORY);
            for (final Category cat: categories) {
                p.setString(1, cat.getName());

                p.executeUpdate();
            }
        }
        finally
        {
            if (p != null)
                p.close();

            conn.close();
        }
    }

    /**
     * Insert a new Category into the database.
     */
    public void newCategory(final Category cat) throws SQLException
    {
        ArrayList<Category> c = new ArrayList<>();
        c.add(cat);
        newCategory(c);
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a list of new Tags into the database.
     */
    public void newTag(final ArrayList<Tag> tags) throws SQLException
    {
        PreparedStatement p = null;
        try
        {
            p = conn.prepareStatement(SQLStatements.INSERT_TAG);
            for (final Tag t: tags) {
                p.setString(1, t.getName());
                p.executeUpdate();
            }
        }
        finally
        {
            if (p != null)
                p.close();

            conn.close();
        }
    }

    /**
     * Insert a new Tag into the database.
     */
    public void newTag(final Tag t) throws SQLException
    {
        ArrayList<Tag> tag = new ArrayList<>();
        tag.add(t);
        newTag(tag);
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a list of new Skills into the database.
     */
    public void newSkill(final ArrayList<Skill> skills) throws SQLException
    {
        PreparedStatement p = null;
        try
        {
            p = conn.prepareStatement(SQLStatements.INSERT_SKILL);
            for (final Skill s: skills) {
                p.setString(1, s.getName());
                p.executeUpdate();
            }
        }
        finally
        {
            if (p != null)
                p.close();

            conn.close();
        }
    }

    /**
     * Insert a new Skill into the database.
     */
    public void newSkill(final Skill s) throws SQLException
    {
        ArrayList<Skill> sk = new ArrayList<>();
        sk.add(s);
        newSkill(sk);
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a new Job into the database.
     */
    public void newJob(final ArrayList<Job> jobs) throws SQLException
    {
        PreparedStatement p = null;
        try
        {
            p = conn.prepareStatement(SQLStatements.INSERT_JOB);
            for (final Job j : jobs) {
                p.setString(1, j.getName());
                p.executeUpdate();
            }
        }
        finally
        {
            if (p != null)
                p.close();

            conn.close();
        }
    }

    /**
     * Insert a new Job into the database.
     */
    public void newJob(final Job j) throws SQLException
    {
        ArrayList<Job> job = new ArrayList<>();
        job.add(j);
        newJob(job);
    }
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a new user into the database, with its correlated skills, if any.
     *
     * WARNING: if a required skill doesn't already exist in the database, throw an error.
     *
     * @param u the user.
     * @param skills list of skill to associate with the user (can be empty or null).
     * @throws SQLException if skills were not already in the database, or something went wrong with the connection with the database.
     */
    public void newUserAccount(final User_Acc u, final ArrayList<Skill> skills) throws SQLException
    {
        PreparedStatement p1 = null;
        PreparedStatement p2 = null;

        try
        {
            p1 = conn.prepareStatement(SQLStatements.INSERT_USER_ACC);
            p1.setString(1, u.getUserName());
            p1.setString(2, u.getName());
            p1.setString(3, u.getSurname());
            p1.setDate(4, u.getDate());
            p1.setString(5, u.getMail());
            p1.setLong(6, u.getPassword());

            p1.execute();

            //Update Has_skill table
            if (skills != null) {
                p2 = conn.prepareStatement(SQLStatements.INSERT_HAS_SKILL); //TODO: make sure all Skill in skills do exist before this action
                p2.setString(2, u.getUserName());
                for (final Skill s : skills) {
                    p2.setString(1, s.getName());

                    p2.executeUpdate();
                }
            }
        }
        finally
        {
            if (p1 != null)
                p1.close();

            if (p2 != null)
                p2.close();

            conn.close();
        }
    }



    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a new question into the database, with it's correlated skills, tags, categories and associated job and company, if any.
     *
     * WARNING: It is assumed that all required base elements (all Tags, Categories, Skills, Company, Job) already exist in the database. If one of them does not, throw SQLException
     *
     * @param q the question.
     * @param skills list of skill to associate with the question (can be empty or null).
     * @param cmp Company (can be null)
     * @param job Job (can be null)
     * @param tags list of Tag (can be empty or null)
     * @param categories list of Category (can be empty or null)
     * @throws SQLException It is assumed that all base elements (all Tags, Categories, Skills, Company, Job) already exist in the database. If one of them does not, throw SQLException
     */
    public void newQuestion( final Question q,
                             final ArrayList<Skill> skills,
                             final Company cmp,
                             final Job job,
                             final ArrayList<Tag> tags,
                             final ArrayList<Category> categories) throws SQLException
    {
        PreparedStatement p1 = null;
        PreparedStatement p2 = null;
        PreparedStatement p3 = null;
        PreparedStatement p4 = null;
        PreparedStatement p5 = null;

        try
        {
            p1 = conn.prepareStatement(SQLStatements.INSERT_QUESTION);
            p1.setString(1, q.getID());
            p1.setString(2, q.getTitle());
            p1.setString(3, q.getText());
            p1.setDate(4, q.getDate());
            p1.setString(5, q.getUser());

            p1.execute();

            //Update Involves table
            if (skills != null) {
                p2 = conn.prepareStatement(SQLStatements.INSERT_INVOLVES);
                p2.setString(1, q.getID());
                for (final Skill s : skills) {
                    p2.setString(2, s.getName());
                    p2.executeUpdate();
                }
            }

            //Update About table
            //TODO: in the sql, job is required NOT NULL, but that is an error
            if (job != null || cmp != null)
            {
                p3 = conn.prepareStatement(SQLStatements.INSERT_ABOUT);
                p3.setString(3, q.getID());
                if (cmp == null) p3.setNull(1,Types.NCHAR); else p3.setString(1, cmp.getID());
                if (job == null) p3.setNull(2, Types.VARCHAR); else p3.setString(2, job.getName());
                p3.execute();
            }

            //Update Has_Tag table
            if (tags != null) {
                p4 = conn.prepareStatement(SQLStatements.INSERT_HAS_TAG);
                p4.setString(2, q.getID());
                for (final Tag t : tags) {
                    p4.setString(1, t.getName());
                    p4.executeUpdate();
                }
            }

            //Update Has_Category table
            if (categories != null) {
                p5 = conn.prepareStatement(SQLStatements.INSERT_HAS_CATEGORY);
                p5.setString(2, q.getID());
                for (final Category cat : categories) {
                    p5.setString(1, cat.getName());
                    p5.executeUpdate();
                }
            }
        }
        finally
        {
            if (p1 != null)
                p1.close();

            if (p2 != null)
                p2.close();

            if (p3 != null)
                p3.close();

            if (p4 != null)
                p4.close();

            if (p5 != null)
                p5.close();

            conn.close();
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a new profile Image into the database.
     */
    public void newImage(final Image i) throws SQLException
    {
        PreparedStatement p = null;
        try
        {
            p = conn.prepareStatement(SQLStatements.INSERT_IMAGE);
            p.setString(1, i.getUser());
            p.setBytes(2, i.getImage());

            p.execute();
        }
        finally
        {
            if (p != null)
                p.close();

            conn.close();
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a new company profile into the database.
     */
    public void newCompany(final Company cmp) throws SQLException
    {
        PreparedStatement p = null;
        try
        {
            p = conn.prepareStatement(SQLStatements.INSERT_COMPANY);
            p.setString(1, cmp.getID());
            p.setString(2, cmp.getName());
            p.setString(3, cmp.getWebsite().toString());

            p.execute();
        }
        finally
        {
            if (p != null)
                p.close();

            conn.close();
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a new answer into the database.
     */
    public void newAnswer(final Answer a, final ArrayList<Answer> quoted_ans) throws SQLException
    {
        PreparedStatement p1 = null;
        PreparedStatement p2 = null;

        try
        {
            p1 = conn.prepareStatement(SQLStatements.INSERT_ANSWER);
            p1.setString(1, a.getID());
            p1.setString(2, a.getText());
            p1.setDate(3, a.getDate());
            p1.setString(4, a.getQuestionID());
            p1.setString(5, a.getUser());

            p1.execute();

            if (quoted_ans != null) { //TODO: fix this, it's broken right now
                p2 = conn.prepareStatement(SQLStatements.INSERT_QUOTE);
                p2.setString(1, a.getID());
                for (final Answer quote : quoted_ans) {
                    p2.setString(2, quote.getID());
                    p2.executeUpdate();
                }
            }
        }
        finally
        {
            if (p1 != null)
                p1.close();

            if (p2 != null)
                p2.close();

            conn.close();
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Insert a new rating into the database.
     */
    public void newVote(final Vote v) throws SQLException
    {
        PreparedStatement p = null;
        try
        {
            p = conn.prepareStatement(SQLStatements.INSERT_VOTE);
            p.setString(1, v.getUser());
            p.setString(2, v.getAnswerID());
            p.setDouble(3, v.getRating());

            p.execute();
        }
        finally
        {
            if (p != null)
                p.close();

            conn.close();
        }
    }
}
