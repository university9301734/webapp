package GNNF.backend.resources;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class AbstractResource {
    protected static final JsonFactory JSON_F;

    static
    {
        JSON_F = new JsonFactory();
        JSON_F.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
        JSON_F.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
    }

    public abstract void toJSON(final OutputStream out) throws IOException;

    //public abstract AbstractResource fromJSON(final InputStream in) throws IOException;
}
