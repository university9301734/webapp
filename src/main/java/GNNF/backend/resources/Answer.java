package GNNF.backend.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;

public final class Answer extends AbstractResource
{
    private final String id, text, question_id, user;
    private final Date d;

    private Answer()
    {
        id = "";
        text = "";
        question_id = "";
        user = "";
        d = new Date(0l);
    }

    public Answer(final String id,
                  final String text,
                  final Date d,
                  final String question_id,
                  final String username)
    {
        this.user = username;
        this.id = id;
        this.d = d;
        this.question_id = question_id;
        this.text = text;
    }

    public final String getUser()
    {
        return user;
    }

    public final String getQuestionID()
    {
        return question_id;
    }

    public final String getText()
    {
        return text;
    }

    public final String getID()
    {
        return id;
    }

    public final Date getDate()
    {
        return d;
    }

    @Override
    public void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_F.createGenerator(out);
        gen.writeStartObject();
        gen.writeFieldName("Answer");
            gen.writeStartObject();
            gen.writeStringField("id", this.id);
            gen.writeStringField("text", this.text);
            gen.writeNumberField("date", this.d.getTime());
            gen.writeStringField("question", this.question_id);
            gen.writeStringField("user_acc", this.user);
            gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
        gen.close();
    }


    public Answer fromJSON(InputStream in) throws IOException {
        String id = null, text = null, question_id = null, user = null;
        Date d = null;

        final JsonParser parser = JSON_F.createParser(in);
        for (String field = parser.nextFieldName(); field != null && !field.equals("Answer"); field = parser.nextFieldName()) {
//            if (field == null)
//                throw new IOException("[ANSWER.fromJSON()] Reached end of JSON before finding Answer object");
        }

        boolean done = false;
        while (parser.nextToken() != JsonToken.END_OBJECT && !done)
        {
            switch (parser.getCurrentName()) {
                case "id":
                    parser.nextToken();
                    id = parser.getText();
                    break;
                case "text":
                    parser.nextToken();
                    text = parser.getText();
                    break;
                case "date":
                    parser.nextToken();
                    d = new Date(parser.getLongValue());
                    break;
                case "question":
                    parser.nextToken();
                    question_id = parser.getText();
                    break;
                case "user_acc":
                    parser.nextToken();
                    user = parser.getText();
                    break;
            }

            done = (id != null && text != null && question_id != null && user != null && d != null);
        }

        parser.close();
        return new Answer(id, text, d, question_id, user);
    }

    @Override
    public String toString() {
        return String.format("[ANSWER] %n\tid: %s %n\ttext: %s %n\tdate: %s %n\tquestion_id: %s %n\tuser_acc: %s %n", id, text, d.toString(), question_id, user);
    }

    @Override
    public boolean equals(Object other)
    {
        return this.id.equals(((Answer)other).id);
    }
}
