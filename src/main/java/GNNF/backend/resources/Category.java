package GNNF.backend.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class Category extends AbstractResource
{
    private final String name;

    private Category(){
        name = "";
    }

    public Category(final String name)
    {
        this.name = name;
    }

    public final String getName()
    {
        return name;
    }

    @Override
    public void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_F.createGenerator(out);
        gen.writeStartObject();
        gen.writeFieldName("Category");
            gen.writeStartObject();
        gen.writeStringField("name", this.name);
            gen.writeEndObject();
        gen.writeEndObject();

        gen.flush();
        gen.close();
    }

    public Category fromJSON(InputStream in) throws IOException {
        String name = null;

        final JsonParser parser = JSON_F.createParser(in);
        for (String field = parser.nextFieldName(); field != null && !field.equals("Category"); field = parser.nextFieldName()) {
//            if (field == null)
//                throw new IOException("[ANSWER.fromJSON()] Reached end of JSON before finding Answer object");
        }

        boolean done = false;
        while (parser.nextToken() != JsonToken.END_OBJECT && !done)
        {
            switch (parser.getCurrentName()) {
                case "name":
                    parser.nextToken();
                    name = parser.getText();
                    break;
            }

            done = (name != null);
        }

        parser.close();
        return new Category(name);
    }

    @Override
    public String toString() {
        return String.format("[CATEGORY] %n\tid: %s %n", name);
    }

    @Override
    public boolean equals(Object other){
        return this.name.equals(((Category)other).getName());
    }
}
