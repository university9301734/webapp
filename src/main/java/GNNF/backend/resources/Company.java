package GNNF.backend.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

public final class Company extends AbstractResource
{
    private final String id;
    private final String name;
    private final URL website;

    private Company(){
        id = "";
        name = "";
        URL tmp;
        try {
            tmp = new URL("");
        }catch (MalformedURLException e)
        {
            tmp = null;
        }
        website = tmp;
    }

    public Company(final String id, final String name, final String website)
    {
        this.id = id;
        this.name = name;
        URL tmp;
        try {
            tmp = new URL(website);
        }catch (MalformedURLException e)
        {
            tmp = null; //website is not of paramount importance
        }
        this.website = tmp;
    }

    public final String getID()
    {
        return id;
    }

    public final String getName()
    {
        return name;
    }

    public final URL getWebsite()
    {
        return website;
    }

    @Override
    public void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_F.createGenerator(out);
        gen.writeStartObject();
        gen.writeFieldName("Company");
            gen.writeStartObject();
            gen.writeStringField("id", this.id);
            gen.writeStringField("name", this.name);
            gen.writeStringField("URL", this.website==null?"u":this.website.toString());
            gen.writeEndObject();
        gen.writeEndObject();

        gen.flush();
        gen.close();
    }


    public Company fromJSON(InputStream in) throws IOException {
        String id = null, name = null, website = null;

        final JsonParser parser = JSON_F.createParser(in);
        for (String field = parser.nextFieldName(); field != null && !field.equals("Company"); field = parser.nextFieldName()) {
//            if (field == null)
//                throw new IOException("[ANSWER.fromJSON()] Reached end of JSON before finding Answer object");
        }

        boolean done = false;
        while (parser.nextToken() != JsonToken.END_OBJECT && !done)
        {
            switch (parser.getCurrentName()) {
                case "id":
                    parser.nextToken();
                    id = parser.getText();
                    break;
                case "name":
                    parser.nextToken();
                    name = parser.getText();
                    break;
                case "URL":
                    parser.nextToken();
                    website = parser.getText();
                    break;
            }

            done = (id != null && name != null && website != null);
        }

        parser.close();

        return new Company(id, name, website);
    }

    @Override
    public String toString(){
        return String.format("[COMPANY] %n\tid: %s %n\tname: %s %n\twebsite: %s %n", id, name, website.toString());
    }

    @Override
    public boolean equals(Object obj) {
        return this.id.equals(((Company)obj).getID());
    }
}
