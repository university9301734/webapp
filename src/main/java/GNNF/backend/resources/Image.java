package GNNF.backend.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Image extends AbstractResource
{
    private final String user;
    private final byte[] img;
    private final String md5;

    private Image()
    {
        this.user = "";
        this.img = new byte[0];

        String md5;
        try {
            md5 = checksum();
        }
        catch(NoSuchAlgorithmException e)
        {
            md5 = dumbChecksum();
        }
        this.md5 = md5;
    }

    public Image(final String username, final byte[] img)
    {
        this.user = username;
        this.img = img;

        String md5;
        try {
            md5 = checksum();
        }
        catch(NoSuchAlgorithmException e)
        {
            md5 = dumbChecksum();
        }
        this.md5 = md5;

    }

    public final String getUser()
    {
        return user;
    }

    public final byte[] getImage()
    {
        return img; //TODO: to img.clone() or not to img.clone(), that is the question ?!?!?!?!?!?
    }

    private String checksum() throws NoSuchAlgorithmException
    {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] digest = md5.digest(img);
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    private String dumbChecksum()
    {
        long hash = 0L;
        boolean flag = true;
        for (final byte b : img)
        {
            if(flag)
                hash += (b & 0xFF) << 8;
            else
                hash += (b & 0xFF);

            flag = !flag;
        }

        //1 complement
        hash = ~hash;
        hash = hash & 0xFFFF;

        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(hash);
        return DatatypeConverter.printHexBinary(buffer.array()).toUpperCase();
    }

    @Override
    public boolean equals(Object obj) {
        return this.user.equals(((Image)obj).user) && this.md5.equals(((Image)obj).md5);
    }

    @Override
    public String toString() {
        return String.format("[IMAGE] %n\tuser: %s %n\tmd5: %s %n", this.user, this.md5);
    }


    public Image fromJSON(InputStream in) throws IOException {
        String user = null, img_base64 = null, md5 = null;

        final JsonParser parser = JSON_F.createParser(in);
        for (String field = parser.nextFieldName(); field != null && !field.equals("Image"); field = parser.nextFieldName()) {
//            if (field == null)
//                throw new IOException("[ANSWER.fromJSON()] Reached end of JSON before finding Answer object");
        }

        boolean done = false;
        while (parser.nextToken() != JsonToken.END_OBJECT && !done)
        {
            switch (parser.getCurrentName()) {
                case "user":
                    parser.nextToken();
                    user = parser.getText();
                    break;
                case "file":
                    parser.nextToken();
                    img_base64 = parser.getText();
                    break;
                case "md5":
                    parser.nextToken();
                    md5 = parser.getText();
                    break;
            }

            done = (user != null && img_base64 != null && md5 != null);
        }

        parser.close();

        Image toReturn =  new Image(user, DatatypeConverter.parseBase64Binary(img_base64));
        if (!toReturn.md5.equals(md5))
            throw new IOException("[IMAGE] Corrupted data!");
        return toReturn;
    }

    @Override
    public void toJSON(OutputStream out) throws IOException {
        String img_base64 = DatatypeConverter.printBase64Binary(img);
        final JsonGenerator gen = JSON_F.createGenerator(out);
        gen.writeStartObject();
        gen.writeFieldName("Image");
            gen.writeStartObject();
            gen.writeStringField("user", this.user);
            gen.writeStringField("file", img_base64);
            gen.writeStringField("md5", this.md5);
            gen.writeEndObject();
        gen.writeEndObject();

        gen.flush();
        gen.close();
    }
}
