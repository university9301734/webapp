package GNNF.backend.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;

public final class Question extends AbstractResource
{
    private final String id, title, text, username;
    private final Date d;

    private Question(){
        this.id = "";
        this.title = "";
        this.username = "";
        this.text = "";
        this.d = new Date(0l);
    }

    public Question( final String id,
                 final String title,
                 final String text,
                 final String username,
                 final Date date)
    {
        this.id = id;
        this.title = title;
        this.username = username;
        this.text = text;
        this.d = date;
    }

    /**
     * @return the Question's ID.
     */
    public final String getID()
    {
        return id;
    }

    /**
     * @return the Question's title.
     */
    public final String getTitle()
    {
        return title;
    }

    /**
     * @return the User_Acc associated to this Question.
     */
    public final String getUser()
    {
        return username;
    }

    /**
     * @return the Question body (more detailed description).
     */
    public final String getText()
    {
        return text;
    }

    /**
     * @return the Question's submission date.
     */
    public final Date getDate()
    {
        return d;
    }

    @Override
    public void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_F.createGenerator(out);
        gen.writeStartObject();
        gen.writeFieldName("Question");
            gen.writeStartObject();
            gen.writeStringField("id", this.id);
            gen.writeStringField("title", this.title);
            gen.writeStringField("username", this.username);
            gen.writeStringField("text", this.text);
            gen.writeNumberField("date", this.d.getTime());
            gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
        gen.close();
    }


    public Question fromJSON(InputStream in) throws IOException {
        String id = null, title = null, username = null, text = null;
        Date d = null;

        final JsonParser parser = JSON_F.createParser(in);
        for (String field = parser.nextFieldName(); field != null && !field.equals("Question"); field = parser.nextFieldName()) {
//            if (field == null)
//                throw new IOException("[ANSWER.fromJSON()] Reached end of JSON before finding Answer object");
        }

        boolean done = false;
        while (parser.nextToken() != JsonToken.END_OBJECT && !done)
        {
            switch (parser.getCurrentName()) {
                case "id":
                    parser.nextToken();
                    id = parser.getText();
                    break;
                case "title":
                    parser.nextToken();
                    title = parser.getText();
                    break;
                case "username":
                    parser.nextToken();
                    username = parser.getText();
                    break;
                case "text":
                    parser.nextToken();
                    text = parser.getText();
                    break;
                case "date":
                    parser.nextToken();
                    d = new Date(parser.getLongValue());
                    break;
            }

            done = (id != null && title != null && username != null && text != null && d != null);
        }

        parser.close();
        return new Question(id, title, username, text, d);
    }

    @Override
    public String toString() {
        return String.format("[QUESTION] %n\tid: %s %n\ttitle: %s %n\tusername: %s %n\ttext: %s %n\tdate: %s %n", id, title, username, text, d.toString());
    }

    @Override
    public boolean equals(Object other)
    {
        return this.id.equals(((Question)other).id);
    }
}
