package GNNF.backend.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;

import static GNNF.backend.resources.AbstractResource.JSON_F;

public final class User_Acc extends AbstractResource
{
    private final String name, surname, username, mail;
    private final long pw;
    private final Date d;

    private User_Acc()
    {
        name = "";
        surname = "";
        username = "";
        mail = "";
        pw = 0L;
        d = new Date(0L);
    }

    public User_Acc(final String name,
                    final String surname,
                    final String username,
                    final String mail,
                    final Date bdate,
                    final long hash_password)
    {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.mail = mail;
        this.d = bdate;
        this.pw = hash_password;
    }

    /**
     * @return the User_Acc's first name.
     */
    public final String getName()
    {
        return name;
    }

    /**
     * @return the User_Acc's second name (surname).
     */
    public final String getSurname()
    {
        return surname;
    }

    /**
     * @return the User_Acc's username.
     */
    public final String getUserName()
    {
        return username;
    }

    /**
     * @return the User_Acc's e-mail.
     */
    public final String getMail()
    {
        return mail;
    }

    /**
     * @return the User_Acc's birth date.
     */
    public final Date getDate()
    {
        return d;
    }

    /**
     * @return the User_Acc's hashed password.
     */
    public final long getPassword()
    {
        return pw;
    }

    @Override
    public void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_F.createGenerator(out);
        gen.writeStartObject();
        gen.writeFieldName("User_Acc");
            gen.writeStartObject();
            gen.writeStringField("name", this.name);
            gen.writeStringField("surname", this.surname);
            gen.writeStringField("username", this.username);
            gen.writeStringField("mail", this.mail);
            gen.writeNumberField("date", this.d.getTime());
            gen.writeNumberField("pw", this.pw);
            gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
        gen.close();
    }


    public static User_Acc fromJSON(InputStream in) throws IOException {
        String name = null, surname = null, username = null, mail=null;
        long pw = 0L;
        Date d = null;

        final JsonParser parser = JSON_F.createParser(in);
        for (String field = parser.nextFieldName(); field != null && !field.equals("User_Acc"); field = parser.nextFieldName()) {
//            if (field == null)
//                throw new IOException("[ANSWER.fromJSON()] Reached end of JSON before finding Answer object");
        }

        boolean done = false;
        while (parser.nextToken() != JsonToken.END_OBJECT && !done)
        {
            switch (parser.getCurrentName()) {
                case "name":
                    parser.nextToken();
                    name = parser.getText();
                    break;
                case "surname":
                    parser.nextToken();
                    surname = parser.getText();
                    break;
                case "date":
                    parser.nextToken();
                    d = new Date(parser.getLongValue());
                    break;
                case "username":
                    parser.nextToken();
                    username = parser.getText();
                    break;
                case "mail":
                    parser.nextToken();
                    mail = parser.getText();
                    break;
                case "pw":
                    parser.nextToken();
                    pw = parser.getLongValue();
                    break;
            }

            done = (name != null && surname != null && username != null && pw != 0L && d != null);
        }

        parser.close();
        return new User_Acc(name, surname, username, mail, d, pw);
    }

    @Override
    public String toString() {
        return String.format("[ANSWER] %n\tname: %s %n\tsurname: %s %n\tusername: %s %n\tmail: %s %n\tdate: %s  %n", name, surname, username, mail, d.toString());
    }

    @Override
    public boolean equals(Object other)
    {
        return this.username.equals(((User_Acc)other).username);
    }
}
