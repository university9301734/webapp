package GNNF.backend.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class Vote extends  AbstractResource
{
    private final String user, ans_id;
    private final double rating;

    private Vote(){
        user = "";
        ans_id = "";
        rating = 0;
    }

    /**
     *
     * @param username the user that voted
     * @param answer_id the answer the vote refers to
     * @param rating a double between 0 and 5, if it exceeds these values, it will be clamped to the nearest one.
     */
    public Vote(final String username, final String answer_id, final double rating)
    {
        this.user = username;
        this.ans_id = answer_id;
        if (rating >= 0 && rating <= 5)
            this.rating = rating;
        else if (rating < 0)
            this.rating = 0;
        else
            this.rating = 5;
    }

    public final String getUser()
    {
        return user;
    }

    public final String getAnswerID()
    {
        return ans_id;
    }

    public final double getRating()
    {
        return rating;
    }

    @Override
    public void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_F.createGenerator(out);
        gen.writeStartObject();
        gen.writeFieldName("Vote");
            gen.writeStartObject();
            gen.writeStringField("user", this.user);
            gen.writeStringField("ans_id", this.ans_id);
            gen.writeNumberField("rating", this.rating);
            gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
        gen.close();
    }


    public Vote fromJSON(InputStream in) throws IOException {
        String ans_id = null, user = null;
        double rating = -1;

        final JsonParser parser = JSON_F.createParser(in);
        for (String field = parser.nextFieldName(); field != null && !field.equals("Vote"); field = parser.nextFieldName()) {
//            if (field == null)
//                throw new IOException("[ANSWER.fromJSON()] Reached end of JSON before finding Answer object");
        }

        boolean done = false;
        while (parser.nextToken() != JsonToken.END_OBJECT && !done)
        {
            switch (parser.getCurrentName()) {
                case "ans_id":
                    parser.nextToken();
                    ans_id = parser.getText();
                    break;
                case "rating":
                    parser.nextToken();
                    rating = parser.getDoubleValue();
                    break;
                case "user":
                    parser.nextToken();
                    user = parser.getText();
                    break;
            }

            done = (ans_id != null && rating != -1 && user != null);
        }

        parser.close();
        return new Vote(user, ans_id, rating);
    }

    @Override
    public String toString() {
        return String.format("[VOTE] %n\tusername: %s %n\tanswer_id: %s %n\trating: %s %n", user, ans_id, rating);
    }

    @Override
    public boolean equals(Object other)
    {
        return this.ans_id.equals(((Vote)other).ans_id) && this.user.equals(((Vote)other).user);
    }
}
