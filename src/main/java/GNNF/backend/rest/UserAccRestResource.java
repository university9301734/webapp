package GNNF.backend.rest;

import GNNF.backend.resources.*;
import GNNF.backend.database.*;
import GNNF.backend.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserAccRestResource extends RestResource {

    /**
     * Creates a new REST resource for managing {@code Employee} resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public UserAccRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Creates a new User Account and stores it into the database
     * @throws IOException
     */
    public void createUserAcc() throws IOException{

        HttpErrorMsg msg = null;

        try{
            final User_Acc user = User_Acc.fromJSON(req.getInputStream());

            //store the UserAccount
            CreateDBEntry dbe = new CreateDBEntry(con);
            dbe.newUserAccount(user, new ArrayList<Skill>());

        }catch (Throwable t){
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.CONFLICT, "Cannot create the User Account: it already exists.", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                msg.toJSON(res.getOutputStream());
            } else{
                msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.INTERNAL_SERVER_ERROR, "Cannot create the User Account: unexpected error.", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                msg.toJSON(res.getOutputStream());
            }
        }
    }


}
