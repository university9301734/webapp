package GNNF.backend.servlets;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;


public abstract class CreateNewResourceAbstractServlet extends HttpServlet
{
    private DataSource ds;

    /**
     * Gets the {@code DataSource} for managing the connection pool to the database.
     *
     * @param config
     *          a {@code ServletConfig} object containing the servlet's
     *          configuration and initialization parameters.
     *
     * @throws ServletException
     *          if an exception has occurred that interferes with the servlet's normal operation
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        InitialContext cxt;
        try
        {
            cxt = new InitialContext();
            ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/404GNNF-database"); //TODO: web.xml names the jdbc pool, context.xml configures it.
                                                                                            // HERE must make sure the lookup name is a proper URI.
        }
        catch (NamingException ne)
        {
            ds = null;
            throw new ServletException(
                    String.format("Impossible to access the connection pool to the database: %s",
                            ne.getMessage()));
        }
    }

    /**
     * Releases the {@code DataSource} for managing the connection pool to the database.
     */
    @Override
    public void destroy() {
        ds = null;
    }

    /**
     * Returns the {@code DataSource} for managing the connection pool to the database.
     *
     * @return the {@code DataSource} for managing the connection pool to the database
     */
    protected final DataSource getDataSource() {
        return ds;
    }
}