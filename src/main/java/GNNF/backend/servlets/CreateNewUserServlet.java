package GNNF.backend.servlets;

import GNNF.backend.database.CreateDBEntry;
import GNNF.backend.resources.User_Acc;
import GNNF.backend.util.HttpErrorMsg;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;

@Deprecated
public class CreateNewUserServlet extends CreateNewResourceAbstractServlet
{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp);
    }

    /**
     * Post to the database the newly created user.
     * @param req http in pipe
     * @param resp http out pipe
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User_Acc userAcc = null;
        boolean error = false;
        HttpErrorMsg e_msg = new HttpErrorMsg(); //Just to avoid null pointers

        try
        {
            userAcc = new User_Acc(
                    req.getParameter("name"),
                    req.getParameter("surname"),
                    req.getParameter("username"),
                    req.getParameter("mail"),
                    new Date(Long.parseLong(req.getParameter("bdate"))), //TODO: check this, maybe getParameter doesn't contain a long
                    Long.parseLong(req.getParameter("pw")) //TODO: this MUST already be the pw's HASH CODE !!!!!!!!
            );

            new CreateDBEntry(getDataSource().getConnection()).newUserAccount(userAcc, null); //TODO: account for skills too.
        }
        catch(SQLException sqle)
        {
            error = true;
            if (sqle.getSQLState().equals("23505"))
                e_msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.BAD_REQUEST,
                        String.format("Username %s alredy in use by someone else.", userAcc != null ? userAcc.getUserName() : "")); //TODO: manage exception (userAcc already exists!!!)
            else
                e_msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.INTERNAL_SERVER_ERROR, "Unexpected error!");
        }
        catch(NumberFormatException nfe)
        {
            error = true;
            e_msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.BAD_REQUEST, "Invalid input parameter while creating the new User_Acc");
        }

        //TODO: hook up the UI, accordingly to error state or not
        resp.setContentType("text/html; charset=utf-8");
        PrintWriter out = resp.getWriter();

        if(error)
        {       }
        else
        {       }

        out.flush();
        out.close();
    }
}
