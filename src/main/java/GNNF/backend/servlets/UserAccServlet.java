package GNNF.backend.servlets;

import GNNF.backend.rest.*;
import GNNF.backend.util.*;
import GNNF.backend.resources.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

public class UserAccServlet extends CreateNewResourceAbstractServlet {

    /**
     * The JSON MIME media type
     */
    private static final String JSON_MEDIA_TYPE = "application/json";

    /**
     * The JSON UTF-8 MIME media type
     */
    private static final String JSON_UTF_8_MEDIA_TYPE = "application/json; charset=utf-8";

    /**
     * The any MIME media type
     */
    private static final String ALL_MEDIA_TYPE = "*/*";

    @Override
    protected final void service(final HttpServletRequest req, final HttpServletResponse res)
            throws ServletException, IOException {

        res.setContentType(JSON_UTF_8_MEDIA_TYPE);
        final OutputStream out = res.getOutputStream();

        try {
            // if the request method and/or the MIME media type are not allowed, return.
            // Appropriate error message sent by {@code checkMethodMediaType}
            if (!checkMethodMediaType(req, res)) {
                return;
            }

            // if the requested resource was an Employee, delegate its processing and return
            if (processUserAcc(req, res)) {
                return;
            }

            // if none of the above process methods succeeds, it means an unknow resource has been requested
            final HttpErrorMsg m = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.BAD_REQUEST, "Unknown resource requested.",
                    String.format("Requested resource is %s.", req.getRequestURI()));
            res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            m.toJSON(out);
        } finally {
            // ensure to always flush and close the output stream
            out.flush();
            out.close();
        }
    }

    /**
     * Checks that the request method and MIME media type are allowed.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request method and the MIME type are allowed; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean checkMethodMediaType(final HttpServletRequest req, final HttpServletResponse res)
            throws IOException {

        final String method = req.getMethod();
        final String contentType = req.getHeader("Content-Type");
        final String accept = req.getHeader("Accept");
        final OutputStream out = res.getOutputStream();

        HttpErrorMsg msg = null;

        if(accept == null){
            msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.BAD_REQUEST, "Output media type not specified.", "Accept request header missing.");
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            msg.toJSON(out);
            return false;
        }

        if(!accept.contains(JSON_MEDIA_TYPE) && !accept.equals(ALL_MEDIA_TYPE)) {
            msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.NOT_ACCEPTABLE, "Unsupported output media type. Resources are represented only in application/json.",
                    String.format("Requested representation is %s.", accept));
            res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            msg.toJSON(out);
            return false;
        }

        switch(method) {
            case "GET":
            case "DELETE":
                // nothing to do
                break;

            case "POST":
            case "PUT":
                if(contentType == null) {
                    msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.BAD_REQUEST, "Input media type not specified.", "Content-Type request header missing.");
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    msg.toJSON(out);
                    return false;
                }

                if(!contentType.contains(JSON_MEDIA_TYPE)) {
                    msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.UNSUPPORTED_MEDIA_TYPE, "Unsupported input media type. Resources are represented only in application/json.", String.format("Submitted representation is %s.", contentType));
                    res.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
                    msg.toJSON(out);
                    return false;
                }

                break;
            default:
                msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.METHOD_NOT_ALLOWED, "Unsupported operation.", String.format("Requested operation %s.", method));
                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                msg.toJSON(out);
                return false;
        }

        return true;
    }

    /**
     * Checks whether the request if for an {@link User_Acc} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code User_Acc}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processUserAcc(HttpServletRequest req, HttpServletResponse res) throws IOException {

        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        HttpErrorMsg msg = null;

        // the requested resource was not an employee
        if(path.lastIndexOf("resources/users") <= 0) {
            return false;
        }

        try{
            path = path.substring(path.lastIndexOf("users")+5);

            // the request URI is: /users
            // if method POST, create user
            if (path.length() == 0 || path.equals("/")) {

                switch (method) {
                    case "POST":
                        new UserAccRestResource(req, res, getDataSource().getConnection()).createUserAcc();
                        break;
                    default:
                        msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.METHOD_NOT_ALLOWED, "Unsupported operation for URI /employee.", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        msg.toJSON(res.getOutputStream());
                        break;
                }

            }
        } catch(Throwable t) {
            msg = new HttpErrorMsg(HttpErrorMsg.ERR_CODES.INTERNAL_SERVER_ERROR, "Unexpected error.", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            msg.toJSON(res.getOutputStream());
        }

        return true;
    }

}
