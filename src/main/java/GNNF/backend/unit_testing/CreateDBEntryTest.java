package GNNF.backend.unit_testing;

import GNNF.backend.database.CreateDBEntry;
import GNNF.backend.resources.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * Execute some test for CreateDBEntry.java
 */
public class CreateDBEntryTest {

    static final String DATABASE = "jdbc:postgresql://localhost:5432/GNNF404-database-test";
    static final String USER = "test";
    static final String PW = "test";

    private static Skill skill;
    private static Job job;
    private static Tag tag;
    private static Category category;
    private static User_Acc user;
    private static Question question;
    private static Image image;
    private static Company company;
    private static Answer answer, answer2, answer3;
    private static Vote vote;


    @BeforeAll
    static  void init() throws FileNotFoundException
    {
        try
        {
            Class.forName("org.postgresql.Driver");

        }catch (ClassNotFoundException e)
        {
            System.out.printf("JDBC Driver not found: %s %n", e.getMessage());
        }

        skill = new Skill("tuttofare");
        job = new Job("webapp1718");
        tag = new Tag("hot-topic");
        category = new Category("default");
        user = new User_Acc("Tony",
                "Smith",
                "the_Tony",
                "the_tony@mail.mmm",
                new java.sql.Date(0L),
                13950970L);
        question = new Question("first-question",
                "COSA STIAMO FACENDO?",
                "COSì TANTE COSE NUOVE, NON SAPPIAMO BENE COME ORIENTARCI",
                "the_Tony",
                new Date(0L));
        image = new Image("the_Tony", new byte[100]);
        company = new Company("aaaaa","404GNNF", "https://bitbucket.org/404groupnamenotfound/webapp2018_project");
        answer =  new Answer("first-answer",
                "LO SI SCOPRIRA' ALL'ESAME",
                new Date(0L),
                "first-question",
                "the_Tony");
        answer2 =  new Answer("second-answer",
                "LO SI SCOPRIRA' ALL'ESAME",
                new Date(0L),
                "first-question",
                "the_Tony");
        answer3 =  new Answer("third-answer",
                "LO SI SCOPRIRA' ALL'ESAME",
                new Date(0L),
                "first-question",
                "the_Tony");
        vote = new Vote("the_Tony", "first-answer", 3);

        Scanner sql_in = new Scanner(new File(System.getProperty("user.dir") + "\\src\\main\\sql\\tables.sql"));

        sql_in.useDelimiter(";");

        Connection con = null;
        Statement s = null;
        while(sql_in.hasNext()) {
            try {
                con = DriverManager.getConnection(DATABASE, USER, PW);
                s = con.createStatement();  // !!! WARNING: as of now, the first sql statement executed wipes !!!
                                            // !!! the entire database clean (see src/main/sql/tables.sql)    !!!
                String sql = sql_in.next();
                s.execute(sql);
            } catch (SQLException e) {
                System.out.println("################### ERROR 1 ####################");
                fail(e.getMessage());
            } finally {
                try {
                    if (s != null)
                        s.close();
                    if (con != null)
                        con.close();
                } catch (SQLException e) {
                    System.out.println("################### ERROR 2 ####################");
                    fail(e.getMessage());
                } finally {
                    con = null;
                    s = null;
                }
            }
        }

        sql_in.close();
    }

    //@Test
    public void testNewSkill() throws Exception
    {
        try {
            init();
        }
        catch(FileNotFoundException e)
        {fail(e.getMessage());}

        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                new CreateDBEntry(con).newSkill(skill);
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new skill] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new skill] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new skill] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
            }
        }
    }

    //@Test
    public void testNewJob() throws Exception
    {
        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                new CreateDBEntry(con).newJob(job);
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new job] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new job] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new job] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
            }
        }
    }

    //@Test
    public void testNewTag() throws Exception
    {
        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                new CreateDBEntry(con).newTag(tag);
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new tag] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new tag] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new tag] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
            }
        }
    }

    //@Test
    public void testNewCategory() throws Exception
    {
        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                new CreateDBEntry(con).newCategory(category);
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new category] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new category] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new category] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
            }
        }
    }

    //@Test
    public void testNewUser() throws Exception
    {
        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                ArrayList<Skill> skills = new ArrayList<>();
                skills.add(skill);

                new CreateDBEntry(con).newUserAccount(user, skills);
            }catch(SQLException e) {
                throw new Exception(String.format("[new user] %s %n", e.getMessage()));
            }
        }
        catch(SQLException e){
            throw new Exception(String.format("[new user] %s %n", e.getMessage()));
        }
        finally {
            try {
                con.close();
            }
            catch (SQLException e)
            { throw new Exception(String.format("[new user] %s %n", e.getMessage())); }
            finally
            {
                con = null;
            }
        }
    }

    //@Test
    public void testNewQuestion() throws Exception
    {
        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            ArrayList<Skill> skills = new ArrayList<>();
            skills.add(new Skill("tuttofare"));

            ArrayList<Tag> tags = new ArrayList<>();
            tags.add(new Tag("hot-topic"));

            ArrayList<Category> ctgs = new ArrayList<>();
            ctgs.add(new Category("default"));

            try {
                new CreateDBEntry(con).newQuestion(
                        question,
                        skills,
                        company,
                        job,
                        tags,
                        ctgs
                );
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new question] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new question] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new question] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
            }
        }
    }

    //@Test
    public void testNewImage() throws Exception
    {
        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                new CreateDBEntry(con).newImage(image);
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new image] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new image] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new image] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
            }
        }
    }

    //@Test
    public void testNewCompany() throws Exception
    {
        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                new CreateDBEntry(con).newCompany(company);
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new company] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new company] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new company] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
            }
        }
    }

    //@Test
    public void testNewAnswer() throws Exception //TODO: test also Quote system
    {
        Connection con = null, con2 = null, con3 = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);
            con2 = DriverManager.getConnection(DATABASE, USER, PW);
            con3 = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                ArrayList<Answer> quotes = new ArrayList<>();
                quotes.add(answer);
                new CreateDBEntry(con).newAnswer(
                        answer,
                        null);
                new CreateDBEntry(con2).newAnswer(
                        answer2,
                        quotes);
                quotes.add(answer2);
                new CreateDBEntry(con3).newAnswer(
                        answer3,
                        quotes);
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new answer] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new answer] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
                if (!con2.isClosed())
                    con2.close();
                if (!con3.isClosed())
                    con3.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new answer] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
                con2 = null;
                con3  = null;
            }
        }
    }

    //@Test
    public void testNewVote() throws Exception
    {
        Connection con = null;
        try{
            con = DriverManager.getConnection(DATABASE, USER, PW);

            try {
                new CreateDBEntry(con).newVote(vote);
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new vote] %s %n", e.getMessage()));
            }
        }
        catch (SQLException e){
            throw new Exception(String.format("[new vote] %s %n", e.getMessage()));
        }
        finally{
            try{
                if (!con.isClosed())
                    con.close();
            }
            catch (SQLException e) {
                throw new Exception(String.format("[new vote] %s %n", e.getMessage()));
            }
            finally
            {
                con = null;
            }
        }
    }

    /**
     * Execute tests in the right order
     */
    @Test
    public void testDatabase()
    {
        //Technically JUnit already provides a way to execute n independent tests and report which of them fail.
        //The problem is that database needs data inserted in a specific order, and JUnit execute tests in random order,
        //hence the need for this big universal test.
        try
        {
            testNewSkill();
            System.out.printf("success: skill %n");
            testNewCategory();
            System.out.printf("success: category %n");
            testNewTag();
            System.out.printf("success: tag %n");
            testNewJob();
            System.out.printf("success: job %n");
            testNewCompany();
            System.out.printf("success: company %n");
            testNewUser();
            System.out.printf("success: user %n");
            testNewImage();
            System.out.printf("success: image %n");
            testNewQuestion();
            System.out.printf("success: question  %n");
            testNewAnswer();
            System.out.printf("success: answer %n");
            testNewVote();
            System.out.printf("success: vote %n");
        }
        catch (Exception e)
        {
            fail(e.getMessage());
        }
    }
}
