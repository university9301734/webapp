Maven uses 'surefire' plugin to execute unit testing. 

By default all public methods decorated with @Test, of all classes named '*Test.java', placed in any folder of the project
are loaded and used for unit testing when invoking

	mvn test
	
every other unit test class that does not follow this pattern must be 
specified in the pom.xml (inside <plugins>...<\plugins> section).