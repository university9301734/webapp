package GNNF.backend.unit_testing;

import GNNF.backend.resources.*;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.fail;

public class ResourcesTest {

    @Test
    public void testAnswer()
    {
        Answer a = new Answer("test-ans", "hello", new Date(3333L), "test-quest", "the_Tony");
        Answer a2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try {
            a.toJSON(out);
            String s = new String(out.toString());
            a2 = a.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e) {
            fail();
        }
        
        if (!a.equals(a2))
            fail(); //this test is fine when linked to database, which enforce UNIQUE on the primary key
            
        if (!a.toString().equals(a2.toString()))
            fail(); //deeper text

        System.out.println(a2.toString());
    }

    @Test
    public void testCategory(){
        Category c = new Category("category-ans");
        Category c2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            c.toJSON(out);
            String s = new String(out.toString());
            c2 = c.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e) {
            fail();
        }

        System.out.println(c2.toString());

        if (!c.equals(c2))
            fail();
    }

    @Test
    public void testCompany(){
        Company c = new Company("boemCompany", "Boem Enterprises", "https://boem.com");
        Company c2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            c.toJSON(out);
            String s = new String(out.toString());
            c2 = c.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e) {
            fail();
        }

        System.out.println(c.toString());

        if (!c.equals(c2))
            fail();
    }

    @Test
    public void testImage()
    {
        Image img = new Image("the_Tony", "ajgboajlanblasnbasajgboajlanblasnbasajgboa".getBytes());
        Image img2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            img.toJSON(out);
            String s = new String(out.toString());
            img2 = img.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e) {
            fail();
        }

        System.out.println(img.toString());

        if (!img.equals(img2))
            fail();
    }

    @Test
    public void testJob(){
        Job j = new Job("undertaker");
        Job j2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            j.toJSON(out);
            String s = new String(out.toString());
            j2 = j.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e){
            fail();
        }
        System.out.println(j.toString());

        if (!j.equals(j2))
            fail();
    }

    @Test
    public void testQuestion(){
        Question q = new Question("question4", "Awesomeness of this project","How awesome is this project?", "the_Tony", new Date(3333L));
        Question q2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            q.toJSON(out);
            String s = new String(out.toString());
            q2 = q.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e) {
            fail();
        }

        System.out.println(q.toString());

        if (!q.equals(q2))
            fail();
    }

    @Test
    public void testSkill(){
        Skill sk = new Skill("touching your nose with your tongue");
        Skill sk2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            sk.toJSON(out);
            String s = new String(out.toString());
            sk2 = sk.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e){
            fail();
        }
        System.out.println(sk.toString());

        if (!sk.equals(sk))
            fail();
    }

    @Test
    public void testTag(){
        Tag t = new Tag("#onlyGoodVibes");
        Tag t2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            t.toJSON(out);
            String s = new String(out.toString());
            t2 = t.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e){
            fail();
        }
        System.out.println(t.toString());

        if (!t2.equals(t))
            fail();
    }

    @Test
    public void testUser_Acc(){
        User_Acc ua = new User_Acc("Anthony", "Dell'Eva", "the_Tony", "anthony.delleva@boementerprises.com", new Date(95,11, 3), 1234L);
        User_Acc ua2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            ua.toJSON(out);
            String s = new String(out.toString());
            ua2 = ua.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e){
            fail();
        }
        System.out.println(ua.toString());

        if (!ua2.equals(ua))
            fail();
    }

    @Test
    public void testVote(){
        Vote v = new Vote("the_Tony", "ans4", 5);
        Vote v2 = null;
        OutputStream out = new ByteArrayOutputStream();
        try{
            v.toJSON(out);
            String s = new String(out.toString());
            v2 = v.fromJSON(new ByteArrayInputStream(s.getBytes()));
        }catch(IOException e){
            fail();
        }
        System.out.println(v.toString());

        if (!v2.equals(v))
            fail();
    }


}
