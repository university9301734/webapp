package GNNF.backend.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Deprecated
public class GenericRow implements Iterable<Object>
{
    protected Map<String, Object> row;

    public GenericRow()
    {
        row = Collections.unmodifiableMap(new HashMap<>());
    }

    public GenericRow(HashMap<String, Object> m)
    {
        row = Collections.unmodifiableMap(new HashMap<>(m));
    }

    public GenericRow(String[] keys, Object[] vals) throws Exception
    {
        if (keys.length != vals.length)
            throw new Exception("Keys and values do not match in length");

        HashMap<String, Object> tmp = new HashMap<>();
        for (int i = 0; i < keys.length; i++)
        {
            tmp.put(keys[i], vals[i]);
        }

        row = Collections.unmodifiableMap(new HashMap<>(tmp));
    }

    public final Object getElement(String key)
    {
        return row.get(key);
    }

    public boolean hasKey(String key)
    {
        return row.containsKey(key);
    }

    @Override
    public Iterator<Object> iterator() {
        Iterator<String> k_it = row.keySet().iterator();
        Iterator<Object> it = new Iterator<Object>() {

            @Override
            public boolean hasNext() {
                return k_it.hasNext();
            }

            @Override
            public final Object next() {
                return row.get(k_it.next());
            }
        };
        return it;
    }
}
