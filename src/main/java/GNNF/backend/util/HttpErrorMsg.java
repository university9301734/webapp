package GNNF.backend.util;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

import java.io.IOException;
import java.io.OutputStream;



public class HttpErrorMsg
{
    public enum ERR_CODES {
        NO_ERROR(-1),
        BAD_REQUEST(400),
        METHOD_NOT_ALLOWED(405),
        NOT_ACCEPTABLE(406),
        UNSUPPORTED_MEDIA_TYPE(415),
        NOT_FOUND(404),
        INTERNAL_SERVER_ERROR(500),
        CONFLICT(409);

        private final int err;

        ERR_CODES(final int code) {
            this.err = code;
        }

        @Override
        public String toString()
        { return String.format("E%3d", err); }
    }

    public final ERR_CODES err_code;
    public final String err_msg;
    public final String err_detail;

    protected static final JsonFactory JSON_FACTORY;

    static {
        // setup the JSON factory
        JSON_FACTORY = new JsonFactory();
        JSON_FACTORY.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
        JSON_FACTORY.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
    }

    /**
     * Carry over some information about an HTTP protocol's error.
     * @param code HTTP error code
     * @param msg short description of the error*
     * @param details additional (and optional) informations about the error
     */
    public HttpErrorMsg(ERR_CODES code, String msg, String details)
    {
        err_code = code;
        err_msg = msg;
        err_detail = details;
    }

    public HttpErrorMsg(ERR_CODES code, String msg)
    {
        this(code, msg, "");
    }

    public HttpErrorMsg(ERR_CODES code)
    {
        this(code, "");
    }

    public HttpErrorMsg()
    {
        this(ERR_CODES.NO_ERROR);
    }

    public final void toJSON(final OutputStream out) throws IOException {
        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();
        jg.writeFieldName("httpmessage");
            jg.writeStartObject();
            jg.writeStringField("errcode", err_code.name());
            jg.writeStringField("errmsg", err_msg);
            jg.writeStringField("errdetail", err_detail);
            jg.writeEndObject();
        jg.writeEndObject();

        jg.flush();
    }

}
