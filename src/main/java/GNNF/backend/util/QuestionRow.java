package GNNF.backend.util;

import java.sql.Timestamp;
import java.util.HashMap;

@Deprecated
public final class QuestionRow extends GenericRow
{
    QuestionRow(final String id,
             final String title,
             final String text,
             final Timestamp ts,
             final String username)
    {
        HashMap<String, Object> tmp = new HashMap<>();
        tmp.put("id", id);
        tmp.put("title", title);
        tmp.put("text", text);
        tmp.put("ts", ts);
        tmp.put("user", username);
        row = java.util.Collections.unmodifiableMap(new HashMap<>(tmp));
    }

    public final String getId()
    {
        return (String) row.get("id");
    }

    public final String getTitle()
    {
        return (String) row.get("title");
    }

    public final String geText()
    {
        return (String) row.get("text");
    }

    public final Timestamp getTimeStamp()
    {
        return (Timestamp) row.get("ts");
    }

    public final String getUser()
    {
        return (String) row.get("user");
    }
}
