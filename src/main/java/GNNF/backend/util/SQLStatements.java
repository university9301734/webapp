package GNNF.backend.util;

/**
 * Group useful SQL statements as strings.
 */
public final class SQLStatements
{
    //------------------------------------------------INSERT statements-------------------------------------------------
    public static final String INSERT_SKILL;
    public static final String INSERT_JOB;
    public static final String INSERT_TAG;
    public static final String INSERT_CATEGORY;
    public static final String INSERT_USER_ACC;
    public static final String INSERT_HAS_SKILL;
    public static final String INSERT_QUESTION;
    public static final String INSERT_INVOLVES;
    public static final String INSERT_HAS_TAG;
    public static final String INSERT_HAS_CATEGORY;
    public static final String INSERT_IMAGE;
    public static final String INSERT_COMPANY;
    public static final String INSERT_ABOUT;
    public static final String INSERT_ANSWER;
    public static final String INSERT_QUOTE;
    public static final String INSERT_VOTE;

    //------------------------------------------------SELECT statements-------------------------------------------------
    public static final String SELECT_ = ""; //TODO


    //------------------------------------------------UPDATE statements-------------------------------------------------
    public static final String UPDATE_QUOTE;
    //TODO

    static
    {
        INSERT_SKILL = "INSERT INTO GNNF404.Skill (Name) VALUES (?)";
        INSERT_JOB = "INSERT INTO GNNF404.Job (Name) VALUES (?)";
        INSERT_TAG = "INSERT INTO GNNF404.Tag (Name) VALUES (?)";
        INSERT_CATEGORY = "INSERT INTO GNNF404.Category (Name) VALUES (?)";
        INSERT_USER_ACC = "INSERT INTO GNNF404.User_Acc (Username, Name, Surname, Birth_date, Mail, Password) VALUES (?,?,?,?,?,?)";
        INSERT_HAS_SKILL = "INSERT INTO GNNF404.Has_Skill (Skill, User_Acc) VALUES (" +
                "(SELECT Name FROM GNNF404.Skill WHERE Name = ?)," +
                "(SELECT Username FROM GNNF404.User_Acc WHERE Username = ?))";
        INSERT_QUESTION = "INSERT INTO GNNF404.Question (ID, Object, Text, Date, User_Acc) VALUES (" +
                "?," +
                "?," +
                "?," +
                "?," +
                "(SELECT Username FROM GNNF404.User_Acc WHERE Username = ?))";
        INSERT_INVOLVES = "INSERT INTO GNNF404.Involves (Question, Skill) VALUES (" +
                "(SELECT ID FROM GNNF404.Question WHERE ID = ?)," +
                "(SELECT Name FROM GNNF404.Skill WHERE Name = ?))";
        INSERT_HAS_TAG = "INSERT INTO GNNF404.Has_Tag (Tag, Question) VALUES (" +
                "(SELECT Name FROM GNNF404.Tag WHERE Name = ?),"+
                "(SELECT ID FROM GNNF404.Question WHERE ID = ?))";
        INSERT_HAS_CATEGORY = "INSERT INTO GNNF404.Has_Category (Category, Question) VALUES (" +
                "(SELECT Name FROM GNNF404.Category WHERE Name = ?),"+
                "(SELECT ID FROM GNNF404.Question WHERE ID = ?))";
        INSERT_IMAGE = "INSERT INTO GNNF404.Image (User_Acc, File) VALUES (" +
                "(SELECT Username FROM GNNF404.User_Acc WHERE Username = ?)," +
                "? )";
        INSERT_COMPANY = "INSERT INTO GNNF404.Company (ID, Name, Website) VALUES (?,?,?)";
        INSERT_ABOUT = "INSERT INTO GNNF404.About (Company, Job, Question) VALUES (" +
                "(SELECT ID FROM GNNF404.Company WHERE ID = ?)," +
                "(SELECT Name FROM GNNF404.Job WHERE Name = ?)," +
                "(SELECT ID FROM GNNF404.Question WHERE ID = ?))";
        INSERT_ANSWER = "INSERT INTO GNNF404.Answer (ID, Text, Date, Question, User_Acc) VALUES (" +
                "?," +
                "?," +
                "?," +
                "(SELECT ID FROM GNNF404.Question WHERE ID = ?)," +
                "(SELECT Username FROM GNNF404.User_Acc WHERE Username = ?))";
        INSERT_QUOTE = "INSERT INTO GNNF404.Quote (Quotes, Quoted_by) VALUES (" +
                "(SELECT ID FROM GNNF404.Answer WHERE ID = ?)," +
                "(SELECT ID FROM GNNF404.Answer WHERE ID = ?))";
        INSERT_VOTE = "INSERT INTO GNNF404.Vote (User_Acc, Answer, Rating) VALUES (" +
                "(SELECT Username FROM GNNF404.User_Acc WHERE Username = ?)," +
                "(SELECT ID FROM GNNF404.Answer WHERE ID = ?)," +
                "? )";



        UPDATE_QUOTE = "UPDATE GNNF404.Quote SET Quoted_by = ? WHERE Quoted = ?";
    }






    //Make constructor private
    private SQLStatements()
    {}


}
