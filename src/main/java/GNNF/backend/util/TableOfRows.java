package GNNF.backend.util;

import java.util.ArrayList;
import java.util.Iterator;

@Deprecated
public final class TableOfRows implements Iterable<GenericRow> {

    protected final ArrayList<GenericRow> table;

    public TableOfRows()
    {
        table = new ArrayList<>();
    }

    public void append(GenericRow row)
    {
        table.add(row);
    }

    public final GenericRow replace(int row_idx, GenericRow new_row) throws IndexOutOfBoundsException
    {
        return table.set(row_idx, new_row);
    }

    public Iterator<GenericRow> iterator()
    {
        return table.iterator();
    }

    public final Object getObject(int row_idx, String name)
    {
        if (!hasRow(row_idx))
            return null;

        if (!hasObject(row_idx, name))
            return null;

        return table.get(row_idx).getElement(name);
    }

    public final GenericRow getRow(int row_idx)
    {
        if (!hasRow(row_idx))
            return null;

        return table.get(row_idx);
    }

    public boolean hasRow(int row_idx)
    {
            return table.size() > row_idx;
    }

    public boolean hasObject(int row_idx, String name)
    {
        if (!hasRow(row_idx))
            return false;

        return table.get(row_idx).hasKey(name);
    }
}

