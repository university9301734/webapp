package GNNF.backend.util;

import java.sql.Date;
import java.util.HashMap;

@Deprecated
public final class UserRow extends GenericRow
{
    public UserRow( final String name,
                 final String surname,
                 final String username,
                 final String mail,
                 final Date bdate,
                 final long hash_password)
    {
        HashMap<String, Object> tmp = new HashMap<>();
        tmp.put("name", name);
        tmp.put("surname", surname);
        tmp.put("username", username);
        tmp.put("mail", mail);
        tmp.put("date", bdate);
        tmp.put("pw", hash_password);
        row = java.util.Collections.unmodifiableMap(new HashMap<>(tmp));
    }

    public final String getName()
    {
        return (String) row.get("name");
    }

    public final String getSurname()
    {
        return (String) row.get("surname");
    }

    public final String getUserName()
    {
        return (String) row.get("username");
    }

    public final String getMail()
    {
        return (String) row.get("mail");
    }

    public final Date getDate()
    {
        return (Date) row.get("date");
    }

    public final long getPassword()
    {
        return (Long) row.get("pw");
    }
}
